var App = {
    socket: null,
    socketURL: 'http://172.28.59.75:8080',
    $el: null,
    $title: null,
    $subtitle: null,
    $phone: null,

    initSocket: function () {
        this.socket = io.connect(this.socketURL);
        this.socket.on('connect', this.onSocketConnect.bind(this));
    },

    render: function () {
        navigator.accelerometer.getCurrentAcceleration(this.onSuccess.bind(this), this.onError.bind(this));

        requestAnimationFrame(this.render.bind(this));
    },

    onSuccess: function (acceleration) {

        // decrease the sensibility of accelerometers
        var velocity = acceleration.x / 500;
        this.socket.emit('updateVelocity', velocity);
    },

    onError: function () {
        console.log('onError!');
        return;
    },
    

    onSocketConnect: function () {
        this.socket.emit('connected');

        this.render();
    },

    onDeviceReady: function () {
        this.$el = document.getElementById('app');
        this.$title = this.$el.querySelector('#appTitle');
        this.$subtitle = this.$el.querySelector('#appSubtitle');
        this.$phone = this.$el.querySelector('#appPhone');

        TweenMax.set([this.$phone], {autoAlpha: 0});

        TweenMax.set(this.$title, {y: 90});
        TweenMax.set(this.$subtitle, {y: 40});

        var tl = new TimelineMax();

        /* fake object to tween */
        var x = {x: 0};

        tl.add(TweenMax.to(x, 0.5 ,{x:50}));

        tl.add(TweenMax.staggerTo([this.$title, this.$subtitle], 0.75 , {
                y: 0,
                force3D: true
            }, 0.25)
        );

        tl.add(TweenMax.to(this.$phone, 1, {
                autoAlpha: 1,

                onComplete: function () {
                    this.$phone.classList.add('is-tilting');
                },
                onCompleteScope: this
            })
        );
    },

    initListener: function () {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    init: function () {
        this.initListener();
        this.initSocket();
    }
};

App.init();

console.log(App);
